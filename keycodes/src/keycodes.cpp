#include <X11/keysym.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/X.h>

#include <xcb/xcb.h>
#include <xcb/xtest.h>
#include <xcb/xcb_keysyms.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


struct _MODIFIERS {
	bool left_shift;
	bool right_shift;
	bool ctrl;
	bool left_ctrl;
	bool right_ctrl;
	bool alt;
	bool left_alt;
	bool right_alt;
	bool super;
	bool left_super;
	bool right_super;
    int  capslock;
};
typedef struct _MODIFIERS MODIFIERS;


struct _CONTEXT {
	Display* display;
    xcb_connection_t* xcb_connection;
    bool keyb_codes_state[256]; // for keycode
    MODIFIERS* modifiers;
};
typedef struct _CONTEXT CONTEXT;

int virtualKey2XKeySym(unsigned int key, int extended);
void keySymbolInfo(Display *display, KeySym ks);

int xcb_connection_init(CONTEXT *context) {
 	int i, screenNum;
	// open the connection to the X server
	xcb_connection_t *conn = xcb_connect(NULL, &screenNum);
	if (conn == NULL) {
		fprintf(stderr, "Unable to open display\n");
		exit(1);
	}

	// xtest init
	xcb_test_get_version_cookie_t cookie = xcb_test_get_version(conn, 2, 1);
	xcb_generic_error_t *err = NULL;
	xcb_test_get_version_reply_t *xtest_reply = xcb_test_get_version_reply(conn, cookie, &err);
	if (xtest_reply) {
		fprintf(stderr, "XTest version %u.%u\n", (unsigned int)xtest_reply->major_version, (unsigned int)xtest_reply->minor_version);
		free(xtest_reply);
	}

	if (err) {
		fprintf(stderr, "XTest version error: %d", (int)err->error_code);
		free(err);
	}

	// get the screen 
	const xcb_setup_t *setup = xcb_get_setup(conn);
	xcb_screen_iterator_t iter = xcb_setup_roots_iterator(setup);

	// I want the screen at index screenNum
	for (i = 0; i < screenNum; ++i) {
		xcb_screen_next(&iter);
	}

	xcb_screen_t *screen = iter.data;

    printf("\n");
    printf("Informations of screen %d:\n", screen->root);
    printf("  width.........: %d\n", screen->width_in_pixels);
    printf("  height........: %d\n", screen->height_in_pixels);
    printf("  white pixel...: %d\n", screen->white_pixel);
    printf("  black pixel...: %d\n", screen->black_pixel);
    printf("\n");
    
    context->xcb_connection = conn;
    return 0;
}

int modifier_key_press(CONTEXT *context, KeyCode kc, KeySym ks) {
    MODIFIERS *modifiers = context->modifiers;
	xcb_window_t none = { XCB_NONE };
    
    switch (ks) {
       case XK_Shift_L: {
            modifiers->left_shift = !modifiers->left_shift;
            if (modifiers->left_shift) {
                printf("SHIFT_L modifier key pressed\n");
            	//xcb_test_fake_input(context->xcb_connection, XCB_KEY_PRESS, kc, XCB_CURRENT_TIME, none, 0, 0, 0);
            } else {
                printf("SHIFT_L modifier key released\n");
            	//xcb_test_fake_input(context->xcb_connection, XCB_KEY_RELEASE, kc, XCB_CURRENT_TIME, none, 0, 0, 0);
            }
            break;
        }
        case XK_Shift_R: {
            modifiers->right_shift = !modifiers->right_shift;
            if (modifiers->right_shift) {
                printf("SHIFT_R modifier key pressed\n");
            	//xcb_test_fake_input(context->xcb_connection, XCB_KEY_PRESS, kc, XCB_CURRENT_TIME, none, 0, 0, 0);
            } else {
                printf("SHIFT_R modifier key released\n");
            	//xcb_test_fake_input(context->xcb_connection, XCB_KEY_RELEASE, kc, XCB_CURRENT_TIME, none, 0, 0, 0);
            }
            break;
        }
        case XK_Caps_Lock: {
            modifiers->capslock = modifiers->capslock + 1;
            if (modifiers->capslock == 2) {
                printf("CAPS_LOCK key pressed\n");
            	xcb_test_fake_input(context->xcb_connection, XCB_KEY_PRESS, kc, XCB_CURRENT_TIME, none, 0, 0, 0);
            } else {
                printf("CAPS_LOCK key released\n");
            	xcb_test_fake_input(context->xcb_connection, XCB_KEY_RELEASE, kc, XCB_CURRENT_TIME, none, 0, 0, 0);
            }
            break;
        }
        default:
            printf("Unknown modifier key press\n");
            break;
    }
    
	xcb_flush(context->xcb_connection);
    return 0;
    
}

int modifier_key_release(CONTEXT *context, KeyCode kc, KeySym ks) {
    MODIFIERS *modifiers = context->modifiers;
    switch (ks) {
        case XK_Shift_L:
            printf("SHIFT_L modifier key release skipped\n");
            break;
        case XK_Shift_R:
            printf("SHIFT_R modifier key release skipped\n");
            break;
        case XK_Caps_Lock:
            modifiers->capslock = modifiers->capslock + 1;
            printf("CAPS_LOCK modifier key release skipped\n");
            break;
            
        default:
            printf("Unknown modifier key release\n");
            break;
    }
	xcb_flush(context->xcb_connection);
    return 0;
}

void characters_input(CONTEXT *context, KeyCode kc, KeySym ks) {
	xcb_window_t none = { XCB_NONE };
    MODIFIERS *modifiers = context->modifiers;
    if (modifiers->right_shift || modifiers->left_shift) {
        KeyCode left_shift = XKeysymToKeycode(context->display, XK_Shift_L);
    	xcb_test_fake_input(context->xcb_connection, XCB_KEY_PRESS, left_shift, XCB_CURRENT_TIME, none, 0, 0, 0);
    }
    
	xcb_test_fake_input(context->xcb_connection, XCB_KEY_PRESS, kc, XCB_CURRENT_TIME, none, 0, 0, 0);
	xcb_test_fake_input(context->xcb_connection, XCB_KEY_RELEASE, kc, XCB_CURRENT_TIME, none, 0, 0, 0);
    
    xcb_flush(context->xcb_connection);
    
}

void key_press(CONTEXT *context, KeyCode kc, KeySym ks) {
    // skip mouse buttons
	if (kc < 8) {
		return;
	}

	context->keyb_codes_state[kc] = true;
    
	keySymbolInfo(context->display, ks);
    unsigned int modifier = 0;
    modifier = XkbKeysymToModifiers(context->display, ks);
    if (modifier) {
        modifier_key_press(context, kc, ks);
        return;
    }

	//xcb_window_t none = { XCB_NONE };
	//xcb_test_fake_input(context->xcb_connection, XCB_KEY_PRESS, kc, XCB_CURRENT_TIME, none, 0, 0, 0);
    characters_input(context, kc, ks);
}

void key_release(CONTEXT *context, KeyCode kc, KeySym ks) {
    // skip mouse buttons
	if (kc < 8) {
		return;
	}

	context->keyb_codes_state[kc] = false;
    
	keySymbolInfo(context->display, ks);
    unsigned int modifier = 0;
    modifier = XkbKeysymToModifiers(context->display, ks);
    if (modifier) {
        modifier_key_release(context, kc, ks);
        return;
    }
	xcb_window_t none = { XCB_NONE };
	xcb_test_fake_input(context->xcb_connection, XCB_KEY_RELEASE, kc, XCB_CURRENT_TIME, none, 0, 0, 0);
}

void mouse_motion(xcb_connection_t *c, int relative, int x, int y) {
	xcb_window_t window = { XCB_NONE };
	if (!relative) {
		window = xcb_setup_roots_iterator(xcb_get_setup(c)).data->root;
	}

	printf("mouse moution: %d %d\n", x, y);
	xcb_test_fake_input(c, XCB_MOTION_NOTIFY, relative, 0, window, x, y, 0);
	xcb_warp_pointer(c, window, window, 0, 0, 0, 0, x, y);
	xcb_flush(c);
}

int mouse_click(xcb_connection_t *c, int mouse_button, int relative) {
	
	int button = 0;
	switch (mouse_button) {
		case 0x01: // left mouse button
			button = mouse_button;	
			printf("left mouse button: %d", button);
			break;
		case 0x02: // right mouse button
			button = mouse_button;
			printf("right mouse button: %d", button);
			break;
		case 0x03: // control-break processing
			button = mouse_button;
			printf("control-break mouse button: %d", button);
			break;
		case 0x04: // middle mouse button (three-button mouse)
			button = mouse_button;
			printf("middle mouse button: %d", button);
			break;
		default:
            printf("unknown mouse button: %d\n", mouse_button);
			return 1;
	}

	xcb_window_t window = { XCB_NONE };
	if (!relative) {
		window = xcb_setup_roots_iterator(xcb_get_setup(c)).data->root;
	}


	xcb_test_fake_input(c, XCB_BUTTON_PRESS, button, 0, window, 0, 0, 0);
	xcb_test_fake_input(c, XCB_BUTTON_RELEASE, button, 0, window, 0, 0, 0);
	xcb_flush(c);
	return 0;
}

void xcb_type_on_window(CONTEXT *context, int vkc_count, int vkc[]) {

	int coord[][2] = {
		{90, 10},
		{90, 10},
		{90, 10},
		{90, 10},
		{140, 10},
		{190, 10},
		{230, 10},
		{260, 10},
	};
	for (int i = 1; i < vkc_count; i++) {
		printf("%d\n", i);
		usleep(1000000);
		xcb_keysym_t ks = (int)virtualKey2XKeySym(vkc[i], 0);
		if (ks == NoSymbol) {
            // if not related key symbol available then it's latin1 or utf code
			ks = vkc[i];
		}

		int relative = 0;
		mouse_motion(context->xcb_connection, relative, coord[i][0], coord[i][1]);
		if (mouse_click(context->xcb_connection, ks, relative) == 0) {
            // it's mouse button
			printf("\n");
		} else {
            // it's keyboard key
			xcb_key_symbols_t *syms = xcb_key_symbols_alloc(context->xcb_connection);
			xcb_keycode_t *c = xcb_key_symbols_get_keycode(syms, ks);

            // TODO: use switch/case for key press/release
			key_press(context, (KeyCode) *c, ks);
			key_release(context, (KeyCode) *c, ks);
		}
		xcb_flush(context->xcb_connection);
	}

}


// Function creates X11 keyboard event
XKeyEvent createKeyEvent(Display *display, Window &win, Window &winRoot,
                         bool press, int keycode, int modifiers) {
    XKeyEvent event;

    event.display = display;
    event.window = win;
    event.root = winRoot;
    event.subwindow = None;
    event.time = CurrentTime;
    event.x = 1;
    event.y = 1;
    event.x_root = 1;
    event.y_root = 1;
    event.same_screen = True;
    event.keycode = keycode;
    event.state = modifiers;

    if (press) {
        event.type = KeyPress;
	} else {
        event.type = KeyRelease;
	}

    return event;
}


static void dump (char *str, int len)
{
    printf("(");
    len--;
    while (len-- > 0)
        printf("%02x ", (unsigned char) *str++);
    printf("%02x)", (unsigned char) *str++);
}

void keySymbolInfo(Display *display, KeySym ks) {
    // Get the root window for the current display.
    Window winRoot = XDefaultRootWindow(display);

    // Find the window which has the current keyboard focus.
    Window winFocus;
    int revert;
    XGetInputFocus(display, &winFocus, &revert);

	XSelectInput(display, winFocus, KeyPressMask|KeyReleaseMask);

	unsigned int modifiers = XkbKeysymToModifiers(display, ks);
	KeyCode kc = XKeysymToKeycode(display, ks);
	XKeyEvent event = createKeyEvent(display, winFocus, winRoot, True, kc, modifiers);
	KeySym sym;
	char str[256+1];
	const char *ksname;
	int nbytes = 0;

	nbytes = XLookupString(&event, str, 256, &sym, NULL);
	if (nbytes < 0) nbytes = 0;
	if (nbytes > 256) nbytes = 256;
	str[nbytes] = '\0';
	printf("XLookupString gives %d bytes: ", nbytes);
	if (nbytes > 0) {
		dump(str, nbytes);
		printf(" \"%s\"\n", str);
	} else {
		printf("\n");
	}

	if (sym == NoSymbol) {
		ksname = "NoSymbol";
	} else {
		ksname = XKeysymToString(sym);
		if (!ksname) {
			ksname = "(no name)";
		}
	}
    printf("    state 0x%x, keycode %u (keysym 0x%lx, %s)\n",
	    event.state, event.keycode, (unsigned long) sym, ksname);
}
// Virtual Key code https://msdn.microsoft.com/en-us/library/windows/desktop/dd375731(v=vs.85).aspx
// Keep it ordered to use binary search in the future
// #define CASE(in_kc, out_kc) case in_kc: { return out_kc; break; }
static const int vkey2x11sym_map[][3] = {
	{0x01, NoSymbol, NoSymbol},	// VK_LBUTTON Left mouse button
	{0x02, NoSymbol, NoSymbol},	// VK_RBUTTON Right mouse button
//	{0x03	// VK_CANCEL Control-break processing
//	{0x04	// VK_MBUTTON Middle mouse button (three-button mouse)
//	{0x05	// VK_XBUTTON1 X1 mouse button
//	{0x06	// VK_XBUTTON2 X2 mouse button
//
//	{0x07	// Undefined

	{0x08, XK_BackSpace, NoSymbol},	// VK_BACK BACKSPACE key
	{0x09, XK_Tab, NoSymbol},	// VK_TAB TAB key
//	{0x0A	// Reserved
//	{0x0B	// Reserved
	{0x0C, XK_Clear, NoSymbol},	// VK_CLEAR CLEAR key
	{0x0D, XK_Return, XK_KP_Enter},	// VK_RETURN ENTER key
//	{0x0E	// Undefined
//	{0x0F	// Undefined
	{0x10, XK_Shift_L, NoSymbol},	// VK_SHIFT SHIFT key
	{0x11, XK_Control_L, XK_Control_R},	// VK_CONTROL CTRL key
	{0x12, XK_Alt_L, XK_Alt_R},	// VK_MENU ALT key
	{0x13, XK_Pause, NoSymbol},	// VK_PAUSE PAUSE key
	{0x14, XK_Caps_Lock, NoSymbol},	// VK_CAPITAL CAPS LOCK key

//	{0x15	// VK_KANA IME Kana mode || VK_HANGUEL IME Hanguel mode (maintained for compatibility; use VK_HANGUL) || VK_HANGUL IME Hangul mode
//	{0x16	// Undefined
//	{0x17	// VK_JUNJA IME Junja mode
//	{0x18	// VK_FINAL IME final mode
//	{0x19	// VK_HANJA IME Hanja mode || VK_KANJI IME Kanji mode
//	{0x1A	// Undefined

	{0x1B, XK_Escape, NoSymbol},	// VK_ESCAPE ESC key

//	{0x1C	// VK_CONVERT IME convert
//	{0x1D	// VK_NONCONVERT IME nonconvert
//	{0x1E	// VK_ACCEPT IME accept
//	{0x1F	// VK_MODECHANGE IME mode change request

//	{// START LATIN1 Unicode U+0020..U+00FF
//	{0x20	// VK_SPACE SPACEBAR
	{0x21, XK_KP_Prior, XK_Prior},	// VK_PRIOR PAGE UP key
	{0x22, XK_KP_Next, XK_Next},	// VK_NEXT PAGE DOWN key
	{0x23, XK_KP_End, XK_End},	// VK_END END key
	{0x24, XK_KP_Home, XK_Home},	// VK_HOME HOME key
	{0x25, XK_KP_Left, XK_Left},	// VK_LEFT LEFT ARROW key
	{0x26, XK_KP_Up, XK_Up},	// VK_UP UP ARROW key
	{0x27, XK_KP_Right, XK_Right},	// VK_RIGHT RIGHT ARROW key
	{0x28, XK_KP_Down, XK_Down},	// VK_DOWN DOWN ARROW key
	{0x29, XK_Select, NoSymbol},	// VK_SELECT SELECT key
	{0x2A, NoSymbol, XK_Print},	// VK_PRINT PRINT key
	{0x2B, XK_Execute, NoSymbol},	// VK_EXECUTE EXECUTE key
	{0x2C, XK_Sys_Req, XK_Print},	// VK_SNAPSHOT PRINT SCREEN key
	{0x2D, XK_KP_Insert, XK_Insert}, // VK_INSERT INS key
	{0x2E, XK_KP_Delete, XK_Delete},	// VK_DELETE DEL key
	{0x2F, XK_Help, NoSymbol},	// VK_HELP HELP key
//	{0x30	// 0 key
//	{0x31	// 1 key
//	{0x32	// 2 key
//	{0x33	// 3 key
//	{0x34	// 4 key
//	{0x35	// 5 key
//	{0x36	// 6 key
//	{0x37	// 7 key
//	{0x38	// 8 key
//	{0x39	// 9 key
//	{0x3A	// Undefined
//	{0x3B	// Undefined
//	{0x3C	// Undefined
//	{0x3D	// Undefined
//	{0x3E	// Undefined
//	{0x3F	// Undefined
//	{0x40	// Undefined
//	{0x41	// A key
//	{0x42	// B key
//	{0x43	// C key
//	{0x44	// D key
//	{0x45	// E key
//	{0x46	// F key
//	{0x47	// G key
//	{0x48	// H key
//	{0x49	// I key
//	{0x4A	// J key
//	{0x4B	// K key
//	{0x4C	// L key
//	{0x4D	// M key
//	{0x4E	// N key
//	{0x4F	// O key
//	{0x50	// P key
//	{0x51	// Q key
//	{0x52	// R key
//	{0x53	// S key
//	{0x54	// T key
//	{0x55	// U key
//	{0x56	// V key
//	{0x57	// W key
//	{0x58	// X key
//	{0x59	// Y key
//	{0x5A	// Z key

	{0x5B, NoSymbol, XK_Super_L},	// VK_LWIN Left Windows key (Natural keyboard)
	{0x5C, NoSymbol, XK_Super_R},	// VK_RWIN Right Windows key (Natural keyboard)
	{0x5D, NoSymbol, XK_Menu},	// VK_APPS Applications key (Natural keyboard)
//	{0x5E	// - Reserved

//	{0x5F	// VK_SLEEP Computer Sleep key

	{0x60, XK_KP_0, NoSymbol},	// VK_NUMPAD0 Numeric keypad 0 key
	{0x61, XK_KP_1, NoSymbol},	// VK_NUMPAD1 Numeric keypad 1 key
	{0x62, XK_KP_2, NoSymbol},	// VK_NUMPAD2 Numeric keypad 2 key
	{0x63, XK_KP_3, NoSymbol},	// VK_NUMPAD3 Numeric keypad 3 key
	{0x64, XK_KP_4, NoSymbol},	// VK_NUMPAD4 Numeric keypad 4 key
	{0x65, XK_KP_5, NoSymbol},	// VK_NUMPAD5 Numeric keypad 5 key
	{0x66, XK_KP_6, NoSymbol},	// VK_NUMPAD6 Numeric keypad 6 key
	{0x67, XK_KP_7, NoSymbol},	// VK_NUMPAD7 Numeric keypad 7 key
	{0x68, XK_KP_8, NoSymbol},	// VK_NUMPAD8 Numeric keypad 8 key
	{0x69, XK_KP_9, NoSymbol},	// VK_NUMPAD9 Numeric keypad 9 key
	{0x6A, XK_KP_Multiply, NoSymbol}, // VK_MULTIPLY Multiply key
	{0x6B, XK_KP_Add, NoSymbol},	// VK_ADD Add key
	{0x6C, XK_KP_Separator, NoSymbol},	// VK_SEPARATOR Separator key
	{0x6D, XK_KP_Subtract, NoSymbol},	// VK_SUBTRACT Subtract key
	{0x6E, XK_KP_Decimal, NoSymbol},	// VK_DECIMAL Decimal key
	{0x6F, NoSymbol, XK_KP_Divide},	// VK_DIVIDE Divide key
	{0x70, XK_F1, NoSymbol},	// VK_F1 F1 key
	{0x71, XK_F2, NoSymbol},	// VK_F2 F2 key
	{0x72, XK_F3, NoSymbol},	// VK_F3 F3 key
	{0x73, XK_F4, NoSymbol},	// VK_F4 F4 key
	{0x74, XK_F5, NoSymbol},	// VK_F5 F5 key
	{0x75, XK_F6, NoSymbol},	// VK_F6 F6 key
	{0x76, XK_F7, NoSymbol},	// VK_F7 F7 key
	{0x77, XK_F8, NoSymbol},	// VK_F8 F8 key
	{0x78, XK_F9, NoSymbol},	// VK_F9 F9 key
	{0x79, XK_F10, NoSymbol},	// VK_F10 F10 key
	{0x7A, XK_F11, NoSymbol},	// VK_F11 F11 key
	{0x7B, XK_F12, NoSymbol},	// VK_F12 F12 key
	{0x7C, XK_F13, NoSymbol},	// VK_F13 F13 key
	{0x7D, XK_F14, NoSymbol},	// VK_F14 F14 key
	{0x7E, XK_F15, NoSymbol},	// VK_F15 F15 key
	{0x7F, XK_F16, NoSymbol},	// VK_F16 F16 key
	{0x80, XK_F17, NoSymbol},	// VK_F17 F17 key
	{0x81, XK_F18, NoSymbol},	// VK_F18 F18 key
	{0x82, XK_F19, NoSymbol},	// VK_F19 F19 key
	{0x83, XK_F20, NoSymbol},	// VK_F20 F20 key
	{0x84, XK_F21, NoSymbol},	// VK_F21 F21 key
	{0x85, XK_F22, NoSymbol},	// VK_F22 F22 key
	{0x86, XK_F23, NoSymbol},	// VK_F23 F23 key
	{0x87, XK_F24, NoSymbol},	// VK_F24 F24 key

//	{0x88	// Unassigned
//	{0x89	// Unassigned
//	{0x8A	// Unassigned
//	{0x8B	// Unassigned
//	{0x8C	// Unassigned
//	{0x8D	// Unassigned
//	{0x8E	// Unassigned
//	{0x8F	// Unassigned

	{0x90, NoSymbol, XK_Num_Lock},	// VK_NUMLOCK NUM LOCK key
	{0x91, XK_Scroll_Lock, NoSymbol},	// VK_SCROLL SCROLL LOCK key

//	{0x92	// OEM specific
//	{0x93	// OEM specific
//	{0x94	// OEM specific
//	{0x95	// OEM specific
//	{0x96	// OEM specific
//	{0x98	// Unassigned
//	{0x99	// Unassigned
//	{0x9A	// Unassigned
//	{0x9B	// Unassigned
//	{0x9C	// Unassigned
//	{0x9D	// Unassigned
//	{0x9E	// Unassigned
//	{0x9F	// Unassigned

	{0xA0, XK_Shift_L, NoSymbol},	// VK_LSHIFT Left SHIFT key
	{0xA1, XK_Shift_R, NoSymbol},	// VK_RSHIFT Right SHIFT key
	{0xA2, XK_Control_L, NoSymbol},	// VK_LCONTROL Left CONTROL key
	{0xA3, XK_Control_R, NoSymbol},	// VK_RCONTROL Right CONTROL key
	{0xA4, XK_Alt_L, NoSymbol},	// VK_LMENU Left MENU key
	{0xA5, XK_Alt_R, NoSymbol},	// VK_RMENU Right MENU key
//	{0xA6	// VK_BROWSER_BACK Browser Back key
//	{0xA7	// VK_BROWSER_FORWARD Browser Forward key
//	{0xA8	// VK_BROWSER_REFRESH Browser Refresh key
//	{0xA9	// VK_BROWSER_STOP Browser Stop key
//	{0xAA	// VK_BROWSER_SEARCH Browser Search key
//	{0xAB	// VK_BROWSER_FAVORITES Browser Favorites key
//	{0xAC	// VK_BROWSER_HOME Browser Start and Home key
//	{0xAD	// VK_VOLUME_MUTE Volume Mute key
//	{0xAE	// VK_VOLUME_DOWN Volume Down key
//	{0xAF	// VK_VOLUME_UP Volume Up key
//	{0xB0	// VK_MEDIA_NEXT_TRACK Next Track key
//	{0xB1	// VK_MEDIA_PREV_TRACK Previous Track key
//	{0xB2	// VK_MEDIA_STOP Stop Media key
//	{0xB3	// VK_MEDIA_PLAY_PAUSE Play/Pause Media key
//	{0xB4	// VK_LAUNCH_MAIL Start Mail key
//	{0xB5	// VK_LAUNCH_MEDIA_SELECT Select Media key
//	{0xB6	// VK_LAUNCH_APP1 Start Application 1 key
//	{0xB7	// VK_LAUNCH_APP2 Start Application 2 key
//	{0xB8	// Reserved
//	{0xB9	// Reserved
//	{0xBA	// VK_OEM_1 Used for miscellaneous characters; it can vary by keyboard. For the US standard keyboard, the ';:' key
//	{0xBB	// VK_OEM_PLUS For any country/region, the '+' key
//	{0xBC	// VK_OEM_COMMA For any country/region, the ',' key
//	{0xBD	// VK_OEM_MINUS For any country/region, the '-' key
//	{0xBE	// VK_OEM_PERIOD For any country/region, the '.' key
//	{0xBF	// VK_OEM_2 Used for miscellaneous characters; it can vary by keyboard. For the US standard keyboard, the '/?' key
//	{0xC0	// VK_OEM_3 Used for miscellaneous characters; it can vary by keyboard. For the US standard keyboard, the '`~' key
//	{0xC1	// Reserved
//	{0xC2	// Reserved
//	{0xC3	// Reserved
//	{0xC4	// Reserved
//	{0xC5	// Reserved
//	{0xC6	// Reserved
//	{0xC7	// Reserved
//	{0xD8	// Unassigned
//	{0xD9	// Unassigned
//	{0xDA	// Unassigned
//	{0xDB	// VK_OEM_4 Used for miscellaneous characters; it can vary by keyboard. For the US standard keyboard, the '[{' key
//	{0xDC	// VK_OEM_5 Used for miscellaneous characters; it can vary by keyboard. For the US standard keyboard, the '\|' key
//	{0xDD	// VK_OEM_6 Used for miscellaneous characters; it can vary by keyboard. For the US standard keyboard, the ']}' key
//	{0xDE	// VK_OEM_7 Used for miscellaneous characters; it can vary by keyboard. For the US standard keyboard, the 'single-quote/double-quote' key
//	{0xDF	// VK_OEM_8 Used for miscellaneous characters; it can vary by keyboard.
//	{0xE0	// Reserved
//	{0xE1	// OEM specific
//	{0xE2	// VK_OEM_102 Either the angle bracket key or the backslash key on the RT 102-key keyboard
//	{0xE3	// OEM specific
//	{0xE4	// OEM specific
//	{0xE5	// VK_PROCESSKEY IME PROCESS key
//	{0xE6	// OEM specific
//	{0xE7	// VK_PACKET Used to pass Unicode characters as if they were keystrokes. The VK_PACKET key is the low word of a 32-bit Virtual Key value used for non-keyboard input methods. For more information, see Remark in KEYBDINPUT, SendInput, WM_KEYDOWN, and WM_KEYUP
//	{0xE8	// Unassigned
//	{0xE9	// OEM specific
//	{0xEA	// OEM specific
//	{0xEB	// OEM specific
//	{0xEC	// OEM specific
//	{0xED	// OEM specific
//	{0xEE	// OEM specific
//	{0xEF	// OEM specific
//	{0xF0	// OEM specific
//	{0xF1	// OEM specific
//	{0xF2	// OEM specific
//	{0xF3	// OEM specific
//	{0xF4	// OEM specific
//	{0xF5	// OEM specific
//	{0xF6	// VK_ATTN Attn key
//	{0xF7	// VK_CRSEL CrSel key
//	{0xF8	// VK_EXSEL ExSel key
//	{0xF9	// VK_EREOF Erase EOF key
//	{0xFA	// VK_PLAY Play key
//	{0xFB	// VK_ZOOM Zoom key
//	{0xFC	// VK_NONAME Reserved
//	{0xFD	// VK_PA1 PA1 key
//	{0xFE	// VK_OEM_CLEAR Clear key
};

int virtualKey2XKeySym(unsigned int key, int extended) {
	for (int i = 0; i < sizeof(vkey2x11sym_map)/sizeof(vkey2x11sym_map[0]); i++) {
		if (key == vkey2x11sym_map[i][0]) {
			if (extended) {
				return vkey2x11sym_map[i][2];
			}
			return vkey2x11sym_map[i][1];
		}
	}
    return NoSymbol;
}


int main(int argc, char *argv[]) {
    int vkcc[256] = {0};
    for (int i = 0; i < argc; i++) {
        vkcc[i] = strtol(argv[i], NULL, 16);
    }

	// provide x11 display
	Display *display = XOpenDisplay(0);
	if (display == NULL) {
		return 1;
	}

	CONTEXT context;
	context.display = display;
    xcb_connection_init(&context);
    MODIFIERS modifiers = {0};
    context.modifiers = &modifiers;
    
    // zero the state
    for (int i = 0; i < 256; i++) {
        context.keyb_codes_state[i] = false;
    }
    
    
	xcb_type_on_window(&context, argc, vkcc);
	
	xcb_disconnect(context.xcb_connection);
	return 0;
}
