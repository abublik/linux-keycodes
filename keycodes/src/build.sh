#!/bin/sh
CFLAGS=`pkg-config --cflags xcb xcb-keysyms xcb-xtest`
LIBS=`pkg-config --libs xcb xcb-keysyms xcb-xtest`
clang++-4.0 -std=c++11 -Wall $CFLAGS keycodes.cpp -lX11 $LIBS -o main 
